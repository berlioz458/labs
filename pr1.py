"""Лабораторная работа №1
	Функция func, принимающая целое положительное число и возвращающая:
	"foo"- если число делится на 3
	"bar"- если число делится на 5
	"foo bar"- если число делится на 3 и на 5
	в остальных случаях возвращается строка с принятым числом.
	
"""

def func(a):
    assert a > 0
    if a % 3 == 0 and a % 5 == 0: 
        return "foo bar"
    elif a % 5 == 0: 
        return "bar"
    elif a % 3 == 0: 
        return "foo"
    else: 
        return str(a)

if __name__ == '__main__':
    assert func(15) == 'foo bar'
    assert func(25) == 'bar'
    assert func(24) == 'foo'
    assert func(17) == '17'
    
